班级：20软工2班
学号：202010414216
姓名：万希
# 实验2：用户及权限管理

## 实验目的

  掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验数据库和用户

  数据库是pdborcl，用户是system、sale和hr

## 实验内容

- Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。


## 实验分析及结果文档说明书
1、以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：
具体操作如下：
$ sqlplus system/123@pdborcl
% 创建角色
CREATE ROLE con_res_role;
% 给角色授予对应的权限
GRANT connect,resource,CREATE VIEW TO con_res_role;
% 创建用户并为其设置密码
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
% 为用户授权并分配空间   授权sale用户访问users表空间，空间限额是50M。
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;

2、新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'wan');
INSERT INTO customers(id,name)VALUES (2,'wan1');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
wan
wan1
3.1 用户hr连接到pdborcl，查询sale授予它的视图customers_view

$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
wan
wan1
 3.2  概要文件设置，用户对多登录时最多只能错误3次
 $ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
3.3 查看表空间的数据库文件，以及每个磁盘占用情况
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;

4 实验结束删除用户和角色
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
以上代码在实际操作中有一定变化，具体详情以图片为准





