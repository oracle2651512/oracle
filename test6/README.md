实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

学号：202010414216，姓名：万希，班级：20软工2班


实验目的和内容

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
设计权限及用户分配方案。至少两个用户。
在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
设计一套数据库的备份方案。


实验步骤
1. 表及表空间设计方案：

表空间设计：
- SYSTEM表空间：存放系统表和索引，不允许用户数据存放在该表空间中。
- USERS表空间：存放用户数据表和索引。
- INDEX表空间：存放用户索引表。

  创建表空间：
  CREATE TABLESPACE users02 DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf'
  SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED,


   CREATE TABLESPACE DATA
DATAFILE '/home/oracle/app/oracle/oradata/orcl1/orclpdb/data01.dbf' SIZE 500M
AUTOEXTEND ON NEXT 100M MAXSIZE 500M;

   CREATE TABLESPACE TESTINDEX
DATAFILE '/home/oracle/app/oracle/oradata/orcl1/orclpdb/index01.dbf' SIZE 200M
AUTOEXTEND ON NEXT 50M MAXSIZE 500M;

表设计：
- 商品表（product）：包含商品ID、商品名称、商品描述、商品价格、商品库存等字段。

CREATE TABLE product (
  product_id NUMBER(10) PRIMARY KEY,
  product_name VARCHAR2(50) NOT NULL,
  product_description VARCHAR2(200),
  product_price NUMBER(10,2) NOT NULL,
  product_stock NUMBER(10) NOT NULL
);


- 订单表（orders）：包含订单ID、用户ID、商品ID、订单时间、订单状态、订单总价等字段。

CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_id VARCHAR2(40 BYTE) NOT NULL 
 , product_id VARCHAR2(40 BYTE) NOT NULL
 , order_date DATE NOT NULL 
 , order_status NUMBER(6, 0) NOT NULL 
 , order_price NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;






- 订单详情表（order_detail）：包含订单ID、商品ID、商品数量、商品单价等字段。

CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);



- 用户表（customers）：包含用户ID、用户名、密码等字段。 

CREATE TABLE customers (id number,name varchar2(50),password VARCHAR2(50) NOT NULL) TABLESPACE "USERS" ;

2. 权限及用户分配方案：

- 创建两个用户：一个是管理员用户（C##sale），一个是普通用户（C##user）。
- 管理员用户拥有所有表的读写权限，普通用户只有商品表和订单表的读写权限。

  创建管理员用户

  CREATE USER C##sale IDENTIFIED BY 123;
  CREATE ROLE con_res_role;
  GRANT connect,resource,CREATE VIEW TO con_res_role;
  GRANT con_res_role TO C##sale;

  创建普通用户

  CREATE USER C##user IDENTIFIED BY 123;
  GRANT CONNECT, RESOURCE TO C##user;


授予管理员用户所有表的权限：

GRANT ALL PRIVILEGES TO C##sale;


授予普通用户商品表和订单表的读写权限：

GRANT SELECT, INSERT, UPDATE, DELETE ON product TO C##user;
GRANT SELECT, INSERT, UPDATE, DELETE ON order TO C##user;


3. 程序包设计：

- 创建一个程序包（sales_pkg），包含以下存储过程和函数：

CREATE OR REPLACE PACKAGE sales_pkg AS
  PROCEDURE add_product(p_name IN VARCHAR2, p_price IN NUMBER, p_description IN VARCHAR2);
  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_price IN NUMBER, p_description IN VARCHAR2);
  PROCEDURE delete_product(p_id IN NUMBER);
  PROCEDURE place_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
  PROCEDURE cancel_order(p_order_id IN NUMBER);
  FUNCTION get_order_by_user(p_user_id IN NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION get_product_by_name(p_name IN VARCHAR2) RETURN SYS_REFCURSOR;
  FUNCTION calculate_order_total(p_order_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/


  - add_product：添加商品信息到商品表。
  - update_product：更新商品信息到商品表。
  - delete_product：从商品表中删除商品信息。
  - place_order：创建订单信息到订单表和订单详情表。
  - cancel_order：取消订单信息。
  - get_order_by_user：根据用户ID获取订单信息。
  - get_product_by_name：根据商品名称获取商品信息。
  - calculate_order_total：计算订单总价。

  CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  PROCEDURE add_product(p_name IN VARCHAR2, p_price IN NUMBER, p_description IN VARCHAR2) AS
  BEGIN
    INSERT INTO products (name, price, description) VALUES (p_name, p_price, p_description);
  END add_product;

  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_price IN NUMBER, p_description IN VARCHAR2) AS
  BEGIN
    UPDATE products SET name = p_name, price = p_price, description = p_description WHERE id = p_id;
  END update_product;

  PROCEDURE delete_product(p_id IN NUMBER) AS
  BEGIN
    DELETE FROM products WHERE id = p_id;
  END delete_product;

  PROCEDURE place_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) AS
    v_order_id NUMBER;
  BEGIN
    INSERT INTO orders (user_id) VALUES (p_user_id) RETURNING id INTO v_order_id;
    INSERT INTO order_details (order_id, product_id, quantity) VALUES (v_order_id, p_product_id, p_quantity);
  END place_order;

  PROCEDURE cancel_order(p_order_id IN NUMBER) AS
  BEGIN
    DELETE FROM order_details WHERE order_id = p_order_id;
    DELETE FROM orders WHERE id = p_order_id;
  END cancel_order;

  FUNCTION get_order_by_user(p_user_id IN NUMBER) RETURN SYS_REFCURSOR AS
    v_cursor SYS_REFCURSOR;
  BEGIN
    OPEN v_cursor FOR
      SELECT o.id, o.created_at, od.product_id, p.name, od.quantity, p.price
      FROM orders o
      JOIN order_details od ON o.id = od.order_id
      JOIN products p ON od.product_id = p.id
      WHERE o.user_id = p_user_id;
    RETURN v_cursor;
  END get_order_by_user;

  FUNCTION get_product_by_name(p_name IN VARCHAR2) RETURN SYS_REFCURSOR AS
    v_cursor SYS_REFCURSOR;
  BEGIN
    OPEN v_cursor FOR
      SELECT id, name, price, description
      FROM products
      WHERE name LIKE '%' || p_name || '%';
    RETURN v_cursor;
  END get_product_by_name;

  FUNCTION calculate_order_total(p_order_id IN NUMBER) RETURN NUMBER AS
    v_total NUMBER;
  BEGIN
    SELECT SUM(od.quantity * p.price) INTO v_total
    FROM order_details od
    JOIN products p ON od.product_id = p.id
    WHERE od.order_id = p_order_id;
    RETURN v_total;
  END calculate_order_total;
END sales_pkg;
/


-- 添加商品信息
EXEC sales_pkg.add_product('iPhone 12', 6999, 'Apple手机');

-- 更新商品信息
EXEC sales_pkg.update_product(1, 'iPhone 12 Pro', 7999, 'Apple手机');

-- 删除商品信息
EXEC sales_pkg.delete_product(1);

-- 创建订单信息
EXEC sales_pkg.place_order(1, 2, 3);

-- 取消订单信息
EXEC sales_pkg.cancel_order(1);

-- 根据用户ID获取订单信息
VAR orders_cursor REFCURSOR;
EXEC :orders_cursor := sales_pkg.get_order_by_user(1);
PRINT orders_cursor;

-- 根据商品名称获取商品信息
VAR products_cursor REFCURSOR;
EXEC :products_cursor := sales_pkg.get_product_by_name('iPhone');
PRINT products_cursor;

-- 计算订单总价
SELECT sales_pkg.calculate_order_total(1) FROM DUAL;



4. 备份方案设计：

- 定期备份数据库，包括数据文件、控制文件和日志文件。
- 将备份文件存放在独立的磁盘上，以防止主磁盘故障导致备份数据丢失。
- 定期测试备份数据的可用性，以确保备份数据可以恢复到正常运行状态。
输出结果如图片所示

